#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#define CHUNK 1024

#ifdef _WIN32
#define OS "win"
// #include <conio.h>
#endif

#ifdef __linux__
#define OS "linux"
#endif

// print help
void help() {
  printf(
      "\n config folder: ($APPDATA or $XDG_CONFIG_HOME)/msailor"
      "\n  quickmark file: ($APPDATA or $XDG_CONFIG_HOME)/msailor/quickmark"
      "\n      file format:"
      "\n          name of the media [url]"
      "\n          name of the media [url]"
      "\n          name of the media [url]"
      "\n          ..."
      "\n  source file: ($APPDATA or $XDG_CONFIG_HOME)/msailor/source"
      "\n      file format:"
      "\n          [url]"
      "\n          [url]"
      "\n          [url]"
      "\n          ..."
      "\n  lists: ($APPDATA or XDG_CONFIG_HOME)/msailor/list/*"
      "\n      same file format as quickmark"

      "\n  console:"
      "\n      ./mediasailor -h/--help"
      "\n      ./mediasailor [url]"
      "\n      ./mediasailor [magnet]"
      "\n      ./mediasailor ':yt [arg]'"
      "\n      ./mediasailor ':ytListImport [arg]'"
      "\n      ./mediasailor ':sc [arg]'"
      "\n      ./mediasailor ':magnet [arg]'"
      "\n      ./mediasailor ':indexwp [arg]'"
      "\n      ..."

      "\n  menu:"
      "\n      :help                   (show help)"
      "\n      :update                 (update app, you need to restart app after that)"
      "\n      :sync                   (sync repositories)"
      "\n      :push                   (push msailor folder to your repository doing a 'commit --amend' and 'push -f')"
      "\n      :config-edit            (open config in the editor configured in config ($EDITOR by default))"
      "\n      :quickmark-add [arg]    (add latest media played to quickmark)"
      "\n      :quickmark-edit [arg]   (open quickmark in the editor configured in config ($EDITOR by default))"
      "\n      :list-add [arg]         (add latest media played to a list)"
      "\n                                      (it will show your lists so you can choose where to add the media, or you can write a new list to be added)"
      "\n                                      (WARNING: magnets do not work properly inside lists)"

      "\n      :list-del [arg]         (delete entire list)"
      "\n      :list-editor [arg]      (open list in the editor configured in config ($EDITOR by default))"

      "\n      :yt [arg]"
      "\n      :ytListImport [arg]     (add yt list to a local list in ~/.msailor/list/  |  only add first 100 videos of a list)"
      "\n                                      (use the link address https://www.youtube.com/playlist?list=...)"

      "\n      :sc [arg]               (soundcloud)"

      "\n      :torrent [arg]"
      "\n      :indexwp [arg]          (if you want all, do not pass arguments)"
      "\n");
}

typedef struct {
  char *userconfigpath;
  char *msailorpath;
  char *configpath;
  char *quickmarkpath;
  char *listpath;
  char *syncpath;
} paths;

// fill paths struct
void fillpaths(paths *p) {
  if (strcmp(OS, "win") + 1)
    p->userconfigpath = getenv("APPDATA");

  if (strcmp(OS, "linux") + 1) {
    p->userconfigpath = getenv("XDG_CONFIG_HOME");
    if (!p->userconfigpath || strcmp(p->userconfigpath, "")) {
      p->userconfigpath = strdup(getenv("HOME"));
      strcat(p->userconfigpath, "/.config");
    }
  }

  p->msailorpath = calloc(sizeof(p->userconfigpath), sizeof(int));
  strcpy(p->msailorpath, p->userconfigpath);
  // p->msailorpath = strdup(p->userconfigpath);
  strcat(p->msailorpath, "/msailor");

  p->configpath = calloc(sizeof(p->msailorpath), sizeof(int));
  strcpy(p->configpath, p->msailorpath);
  // p->configpath = strdup(p->msailorpath);
  strcat(p->configpath, "/config");

  p->quickmarkpath = calloc(sizeof(p->msailorpath), sizeof(int));
  strcpy(p->quickmarkpath, p->msailorpath);
  // p->quickmarkpath = strdup(p->msailorpath);
  strcat(p->quickmarkpath, "/quickmark");

  p->listpath = calloc(sizeof(p->msailorpath), sizeof(int));
  strcpy(p->listpath, p->msailorpath);
  // p->listpath = strdup(p->msailorpath);
  strcat(p->listpath, "/list");

  p->syncpath = calloc(sizeof(p->msailorpath), sizeof(int));
  strcpy(p->syncpath, p->msailorpath);
  // p->syncpath = strdup(p->msailorpath);
  strcat(p->syncpath, "/sync");
}

// push changes to user's repo
void push(paths *p) {
  char *command = calloc(
      sizeof("git -C ")
      + sizeof(p->msailorpath)
      + sizeof(" stage . && git -C ")
      + sizeof(p->msailorpath)
      + sizeof(" commit -m 'msailor auto push' && git -C ")
      + sizeof(p->msailorpath)
      + sizeof(" push")
      , sizeof(int));
  strcpy(command, "git -C ");
  strcat(command, p->msailorpath);
  strcat(command, " stage . && git -C ");
  strcat(command, p->msailorpath);
  strcat(command, " commit -m 'msailor auto push' && git -C ");
  strcat(command, p->msailorpath);
  strcat(command, " push");
  system(command);
  free(command);
}

// sync repos
void syncrepos(char *repo, paths *p) {
  char *command = calloc(
      sizeof("git clone") + sizeof(repo) + sizeof(p->syncpath), sizeof(int));
  strcpy(command, "git clone ");
  strcat(command, repo);
  strcat(command, " ");

  char *token;
  int i = 0;
  while ((token = strsep(&repo, "/"))) {
    if (i == 3) {
      size_t sz = strlen(command);
      if (sz > 0 && command[sz - 2] == '\n')
        command[sz - 2] = ' ';
      strcat(command, p->syncpath);
      strcat(command, "/");
      strcat(command, token);
      system(command);
    }
    i++;
  }
  free(command);
  free(token);
}

// read config file and execute it
void config(paths *p) {
  // remove sync dir
  if (strcmp(OS, "win") + 1) {
    char *command = calloc(sizeof("rmdir /S ") + sizeof(p->syncpath), sizeof(int));
    strcpy(command, "rmdir /S ");
    strcat(command, p->syncpath);
    system(command);
    free(command);
  }
  if (strcmp(OS, "linux") + 1) {
    char *command = calloc(sizeof("rm -rf ") + sizeof(p->syncpath), sizeof(int));
    strcpy(command, "rm -rf ");
    strcat(command, p->syncpath);
    system(command);
    free(command);
  }
  // create sync dir
  mkdir(p->syncpath, 0755);

  char *line = NULL;
  FILE *file;
  size_t len;
  file = fopen(p->configpath, "r");
  if (file) {
    while ((getline(&line, &len, file)) > 0)
      if (strstr(line, "sync="))
        syncrepos(line + 5, p);
    if (ferror(file))
      printf("Error opening config file");
    fclose(file);
  }
  free(line);
}

// edit config file
void configedit(paths *p) {
  if (strcmp(OS, "win") + 1) {
    char *command = calloc(sizeof("notepad ") + sizeof(p->configpath), sizeof(int));
    strcpy(command, "notepad ");
    strcat(command, p->configpath);
    system(command);
    free(command);
  }
  if (strcmp(OS, "linux") + 1) {
    char *command = calloc(sizeof("$EDITOR ") + sizeof(p->configpath), sizeof(int));
    strcpy(command, "$EDITOR ");
    strcat(command, p->configpath);
    system(command);
    free(command);
  }
}

// add latest media played to quickmark
void quickmarkadd(paths *p, char *entry) {
  char *line = NULL;
  FILE *file;
  size_t len;
  // create if it does not exists
  file = fopen(p->quickmarkpath, "w");
  if(file)
    fclose(file);
  // append to file
  file = fopen(p->quickmarkpath, "a");
  if (file) {
    fprintf(file, "%s\n", entry);
    if (ferror(file))
      printf("Error adding quickmark: %s", entry);
    fclose(file);
  }
  free(line);
}

// edit quickmark
void quickmarkedit(paths *p) {
  if (strcmp(OS, "win") + 1) {
    char *command = calloc(sizeof("notepad ") + sizeof(p->quickmarkpath), sizeof(int));
    strcpy(command, "notepad ");
    strcat(command, p->quickmarkpath);
    system(command);
    free(command);
  }
  if (strcmp(OS, "linux") + 1) {
    char *command = calloc(sizeof("$EDITOR ") + sizeof(p->quickmarkpath), sizeof(int));
    strcpy(command, "$EDITOR ");
    strcat(command, p->quickmarkpath);
    system(command);
    free(command);
  }
}

// add entry to list
void listadd(paths *p, char *list, char *entry) {
  char *line = NULL;
  FILE *file;
  size_t len;
  char *filepath = calloc(sizeof(p->listpath) + sizeof("/") + sizeof(list), sizeof(int));
  strcpy(filepath, p->listpath);
  strcat(filepath, "/");
  strcat(filepath, list);
  // create if it does not exists
  file = fopen(filepath, "w");
  if(file)
    fclose(file);
  // append to file
  file = fopen(filepath, "a");
  if (file) {
    fprintf(file, "%s\n", entry);
    if (ferror(file))
      printf("Error adding to %s: %s", list, entry);
    fclose(file);
  }
  free(line);
  free(filepath);
}

// edit list
void listedit(paths *p, char *list) {
  if (strcmp(OS, "win") + 1) {
    char *command = calloc(sizeof("notepad ") + sizeof(p->listpath) + sizeof("/") + sizeof(list), sizeof(int));
    strcpy(command, "notepad ");
    strcat(command, p->listpath);
    strcat(command, "/");
    strcat(command, list);
    system(command);
    free(command);
  }
  if (strcmp(OS, "linux") + 1) {
    char *command = calloc(sizeof("$EDITOR ") + sizeof(p->listpath) + sizeof("/") + sizeof(list), sizeof(int));
    strcpy(command, "$EDITOR ");
    strcat(command, p->listpath);
    strcat(command, "/");
    strcat(command, list);
    system(command);
    free(command);
  }
}

// clear terminal
void clearterminal() {
  for(int i = 0; i < 100; i++)
    printf(" \n");
}

// generate a terminal menu to filter between options given and return the chosen one.
char* menu(char *options[]) {
  int optionslength = sizeof(options) / sizeof(*options);
  char *command;
  char *currentselection[] = {};


  while(1) {
    if(command == NULL || strcmp(command, "")) {
      for(int i = 0; i < optionslength; i++)
        memcpy(currentselection[i], &options[i], strlen(options[i])+1);
    } else {
      for(int i = 0; i < optionslength; i++)
        if (strstr(options[i], command))
          memcpy(currentselection[i], &options[i], strlen(options[i])+1);
    }

    for(int i = 0; i < sizeof(currentselection); i++)
      printf("%s\n", currentselection[i]);

    printf("\nwrite \"help\" to get the list of available commands and their description\n");
    printf("hibro: ");
    if(command != NULL)
      printf("%s\n", command);

    // OSX
    // system("stty raw");


    // use conio.h in windows for getch since windows does not allow stty changes
    /* if (strcmp(OS, "win") + 1) {
       char stdin = _getch();
       } */

    system("/bin/stty raw");
    char stdin = getchar();
    // creating a string from stdin so it can be concatenated to command
    char ch[2] = "\0";
    ch[0] = stdin;
    system("/bin/stty cooked");
    // clearterminal();
    switch((int)stdin) {
      case 27: // esc
        memcpy(command, "", strlen("")+1);
        break;
      case 127: // return
        command[strlen(command)-1] = '\0';
        return command;
        break;
      case 13: // enter
        printf("enter pressed");
        break;
      case 9: // tab
        for(int i = 0; i < optionslength; i++)
          if (strstr(options[i], command))
            memcpy(command, &options[i], strlen(options[i])+1);
        break;

      default:
        printf("yeah\n");
        if(command == NULL) {
          printf("default command null");
          memcpy(command, ch, strlen(ch)+1);
        } else {
          printf("default command not null");
          char *aux;
          memcpy(aux, command, strlen(command)+1);
          strcat(aux, ch);
          memcpy(command, aux, strlen(aux)+1);
          free(aux);
        }
        break;
    }

    // break;
  }

  free(command);
  return NULL;
}

// this is obviously not the main
int main() {
  paths *p = calloc(sizeof(paths), sizeof(int));
  fillpaths(p);

  // create dirs
  mkdir(p->msailorpath, 0755);
  mkdir(p->syncpath, 0755);

  // config(p);
  // help();
  // push(p);
  // configedit(p);
  // quickmarkedit(p);

  char *example[] = { "1", "2", "3", "4" };
  menu(example);

  // free(p);

  return 0;
}
